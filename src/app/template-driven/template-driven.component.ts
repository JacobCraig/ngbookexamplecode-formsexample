import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'template-driven',
  templateUrl: './template-driven.component.html',
  styleUrls: ['./template-driven.component.scss']
})
export class TemplateDrivenComponent implements OnInit {
  user: User = {
    name: 'Todd Motto',
    account: {
      email: 'testemail@email.com',
      confirm: ''
    }
  };

  public onSubmit = (form: NgForm) => {
    console.log('Submitted form: ', form.value);
  }

  constructor() { }

  ngOnInit() {
  }

}

interface User {
  name: string;
  account: {
    email: string;
    confirm: string;
  }
}