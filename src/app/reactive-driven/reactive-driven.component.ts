import { Component } from '@angular/core';

@Component({
  selector: 'reactive-driven',
  templateUrl: './reactive-driven.component.html',
  styleUrls: ['./reactive-driven.component.scss']
})

export class ReactiveDrivenComponent {
  public onSubmit = (formInput: any) => {
    console.log('Submitted form: ', formInput);
  }
}
